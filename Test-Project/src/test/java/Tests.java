
//import java.util.ArrayList;
import java.time.Duration;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Tests {
	@Test(groups = { "test-one" })
	public void draganddrop() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		System.out.println("hellow");
		driver.get("https://jqueryui.com/droppable");
		Thread.sleep(3000);
		System.out.println(driver.getTitle());
		List<WebElement> framesArrayList = driver.findElements(By.tagName("iframe"));
		System.out.println(framesArrayList.size());
		driver.switchTo().frame(0);
		System.out.println();
		WebElement srcElement = driver.findElement(By.id("draggable"));
		WebElement tragetElement = driver.findElement(By.id("droppable"));
		Actions actions = new Actions(driver);
		actions.dragAndDrop(srcElement, tragetElement).build().perform();
		driver.get("https://www.wikipedia.org/");
		driver.findElement(By.cssSelector(".app-badge a")).click();
		Set<String> windowSet = driver.getWindowHandles();
		Iterator<String> iterator = windowSet.iterator();
		String parantString = iterator.next();
		String childString = iterator.next();
		System.out.println(driver.switchTo().window(childString).getTitle());
		driver.close();

	}

	@Test
	public void checkBox() {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		System.out.println(driver.getCurrentUrl());
		driver.findElement(By.cssSelector("input[id='checkBoxOption1']")).click();
		System.out.println(driver.findElement(By.cssSelector("input[id='checkBoxOption1']")).isSelected());

		driver.findElement(By.cssSelector("input[id='checkBoxOption1']")).click();
		System.out.println(driver.findElement(By.cssSelector("input[id='checkBoxOption1']")).isSelected());
		List<WebElement> tElements = driver.findElements(By.cssSelector("input[type='checkbox']"));
		System.out.println(tElements.size());
		// driver.close();
	}

	@Test
	public void airlineWebsite() {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		driver.get("https://www.cleartrip.com/");
		Select select = new Select(driver.findElement(By.cssSelector("div[class='mb-4'] select")));
		select.selectByValue("5");
		// System.out.println(driver.findElement(By.cssSelector("div[class='mb-4']
		// select")).getText());
		try {
			getDateElement(driver, "20", "June 2022").click();
		} catch (Exception e) {
			System.err.println("Month Year Cound not be found");
		}

	}

	public WebElement getDateElement(WebDriver driver, String dateString, String monthyear) {
		WebElement datElement = null;
		WebElement monthElement = null;

		driver.findElement(By.cssSelector("svg[class='t-all ml-2'] g")).click();
		boolean isMonthFound = false;
		while (!isMonthFound) {
			List<WebElement> monthsElements = driver.findElements(By.cssSelector("div[class='DayPicker-Month']"));
			for (WebElement element : monthsElements) {
				if (element.findElement(By.cssSelector(".DayPicker-Caption")).getText().equals(monthyear)) {
					monthElement = element;
					isMonthFound = true;
					break;
				}
			}
			if (isMonthFound != true) {
				driver.findElement(By.cssSelector("svg[data-testid='rightArrow']")).click();
			}
		}
		if (isMonthFound == true) {
			List<WebElement> datesElements = monthElement.findElements(By.cssSelector("div[class*='DayPicker-Day']"));
			System.out.println(datesElements.size());
			for (WebElement element : datesElements) {
				if (element.getText().equals(dateString)) {
					datElement = element;
				}
			}
		}
		return datElement;
	}

//-----------------------------------------------------------------------------------	
	// Assignment 3
	@Test
	public void explicitwait() {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.itgeared.com/demo/1506-ajax-loading.html");
		driver.findElement(By.linkText("Click to load get data via Ajax!")).click();
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("results")));
		System.out.println(driver.findElement(By.id("results")).getText());
	}

	// Assignment 4
	@Test
	public void windowHandling() {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		driver.get("https://the-internet.herokuapp.com");
		driver.findElement(By.linkText("Multiple Windows")).click();
		driver.findElement(By.linkText("Click Here")).click();
		Set<String> windowSet = driver.getWindowHandles();
		Iterator<String> iterator = windowSet.iterator();
		String paraentString = iterator.next();
		String chilString = iterator.next();
		driver.switchTo().window(chilString);
		System.out.println(driver.findElement(By.tagName("h3")).getText());
		driver.switchTo().window(paraentString);
		System.out.println(driver.findElement(By.tagName("h3")).getText());
		driver.quit();
	}

	// Assignment 5
	@Test
	public void framesDemo() {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("https://the-internet.herokuapp.com/nested_frames");
		driver.switchTo().frame(0);
		List<WebElement> subFrame = driver.findElements(By.tagName("frame"));
		for (int i = 0; i < subFrame.size(); i++) {
			driver.switchTo().frame(i);
			System.out.println(driver.findElement(By.tagName("body")).getText());
			driver.switchTo().parentFrame();
		}
		driver.switchTo().parentFrame();
		driver.switchTo().frame(1);
		System.out.println(driver.findElement(By.tagName("body")).getText());
		driver.close();
	}

	// Assignment 6
	@Test
	public void mixExterices() {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));

		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		String tString = selectCheckbox("2", driver);
		SelectDropdown(tString, driver);
		alertTest(tString, driver);
		driver.quit();
	}

	public String selectCheckbox(String number, WebDriver driver) {
		String checkboxText = null;
		driver.findElement(By.id("checkBoxOption" + number)).click();
		checkboxText = driver.findElement(By.xpath("//input[@id='checkBoxOption" + number + "']/parent::label"))
				.getText();
		return checkboxText;
	}

	public void SelectDropdown(String option, WebDriver driver) {
		Select optionSelect = new Select(driver.findElement(By.id("dropdown-class-example")));
		optionSelect.selectByVisibleText(option);
	}

	public void alertTest(String name, WebDriver driver) {
		System.out.println(name);
		driver.findElement(By.id("name")).sendKeys(name);
		driver.findElement(By.id("alertbtn")).click();
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		Assert.assertTrue((alert.getText().contains(name)));
		alert.accept();
	}
//----------------------------------------------------

	// Assignment 7
	@Test
	public void tableInteraction() {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		List<WebElement> rows = driver.findElements(By.xpath("//div[@class='left-align']/fieldset/table/tbody/tr"));
		System.out.println(rows.size());

		for (int i = 0; i < 3; i++) {
			System.out.println(rows.get(4).findElements(By.tagName("td")).get(i).getText());
		}
		driver.close();
	}

	@Test
	public void autoSuggestiveDropbox() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/Users/bhavan/Downloads/Webdrivers/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		driver.get("https://rahulshettyacademy.com/AutomationPractice/");
		driver.findElement(By.id("autocomplete")).sendKeys("uni");
		driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
		driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
		driver.findElement(By.id("autocomplete")).sendKeys(Keys.DOWN);
		System.out.println(driver.findElement(By.id("autocomplete")).getAttribute("value"));
	}
}
